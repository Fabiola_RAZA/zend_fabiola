<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2014 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Contact\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Contact\Model\ContactQuery;
use Contact\Model\Contact;

class IndexController extends AbstractActionController
{
    public function indexAction()
    {
        return new ViewModel();
    }
    
    public function dashboardAction()
    {
        $collectionContacts = ContactQuery::create()->find();
        return new ViewModel(array("collectionContact" => $collectionContacts));
    }
    
    public function consulterAction()
    {
        $contactSelectionne = ContactQuery::create()->findOneById($this->params('id'));
        return new ViewModel(['unContact' => $contactSelectionne]);
    }
    
    public function ajouterAction()
    {
        if ($this->getRequest()->isPost())
        {
            $contact = new Contact();
            $nom = $this->getRequest()->getPost('nom');
            $prenom = $this->getRequest()->getPost('prenom');
            $email = $this->getRequest()->getPost('email');
            $telephone = $this->getRequest()->getPost('telephone');
            $contact->setEmail($email);
            $contact->setNom($nom);
            $contact->setPrenom($prenom);
            $contact->setTelephone($telephone);
            $contact->save();
            return $this->redirect()->toRoute('rContact-Gestion',array('action' => "dashboard"));
        }
        return new ViewModel();
    }
    
    public function modifierAction()
    {
        if ($this->getRequest()->isPost())
        {
            $contactSelectionne = ContactQuery::create()->findOneById($this->getRequest()->getPost('id'));
            $nom = $this->getRequest()->getPost('nom');
            $prenom = $this->getRequest()->getPost('prenom');
            $email = $this->getRequest()->getPost('email');
            $telephone = $this->getRequest()->getPost('telephone');
            $contactSelectionne->setEmail($email);
            $contactSelectionne->setNom($nom);
            $contactSelectionne->setPrenom($prenom);
            $contactSelectionne->setTelephone($telephone);
            $contactSelectionne->save();
        }
        else
        {
            $contactSelectionne = ContactQuery::create()->findOneById($this->params('id'));
        }
        return new ViewModel(['unContact' => $contactSelectionne]);
    }
    
    public function supprimerAction()
    {
        $contact = ContactQuery::create()->findOneById($this->params('id'));
        $contact->delete();
        return $this->redirect()->toRoute('rContact-Gestion',array('action' => "dashboard")); //redirection, donc ici recharge la page
    }
}
